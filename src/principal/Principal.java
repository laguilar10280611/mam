
package principal;

/**
 *
 * @author Armand
 */
import javax.persistence.*;
import entidad.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
public class Principal 
{
    public static void main(String args[])
    {
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("Linea-OrdenPU");
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        DateFormat FormatoFecha = new SimpleDateFormat("dd/MM/yyyy");
        Date fecha = null;
        try
        {
             fecha = FormatoFecha.parse("21/11/2011");
        }
        catch(Exception e)
        {
            System.out.println("ERROR AL CONVERTIR LA FECHA");
        }
        
        //PRODUCTOS
        Producto prod1 = new Producto();
        prod1.setIdProducto(1);
        prod1.setDescripcion("GEL PARA QUE TU PEINADO DURE 24 HRS");
        prod1.setPrecio(34.99f);
        
        Producto prod2 = new Producto();
        prod2.setIdProducto(2);
        prod2.setDescripcion("PASTILLAS PARA LA GRIPA 500 mg");
        prod2.setPrecio(99.50f);
        
        Producto prod3 = new Producto();
        prod3.setIdProducto(3);
        prod3.setDescripcion("VENDAS ELASTICAS PARA DEPORTES 7mm");
        prod3.setPrecio(15.87f);
        
        Producto prod4 = new Producto();
        prod4.setIdProducto(4);
        prod4.setDescripcion("JARABE SABOR FRESA PARA LA TOS SECA");
        prod4.setPrecio(99.50f);
        
        //ORDENES
        Orden orden1= new Orden();
        orden1.setIdOrden(1);
        orden1.setFechaOrden(fecha);
        
        Orden orden2= new Orden();
        orden2.setIdOrden(2);
        orden2.setFechaOrden(fecha);
        
        Orden orden3= new Orden();
        orden3.setIdOrden(3);
        orden3.setFechaOrden(fecha);
        
        //LINEAS DE ORDEN
        Lineaorden lineord1= new Lineaorden(1,1);
        lineord1.setCantidad(4); 
//       LineaordenPK lordn= new LineaordenPK(1,1);
//       lineord1.setLineaordenPK(lordn);
//        
        lineord1.setOrden(orden1);
        lineord1.setProducto(prod1);
        
        Lineaorden lineord2= new Lineaorden(1,2);
        lineord2.setCantidad(2); 
        lineord2.setOrden(orden1);
        lineord2.setProducto(prod2);
        
        Lineaorden lineord3= new Lineaorden(2,1);
        lineord3.setCantidad(1); 
        lineord3.setOrden(orden2);
        lineord3.setProducto(prod1);
        
        Lineaorden lineord4= new Lineaorden(2,2);
        lineord4.setCantidad(6); 
        lineord4.setOrden(orden2);
        lineord4.setProducto(prod2);
        
        try {
            em.persist(prod1);
            em.persist(prod2);
            em.persist(prod3);
            em.persist(prod4);
            
            em.persist(orden1);
            em.persist(orden2);
            em.persist(orden3);
            
            em.persist(lineord1);
            em.persist(lineord2);
            em.persist(lineord3);
            em.persist(lineord4);
           
            
        } catch (Exception e) {
            System.out.println("ERROR AL REGISTRAR DATOS!!!" + e.getMessage()); 

        }
        
        System.out.println("=========== IMPRIMIENDO CATALOGO DE PRODUCTOS =========== ");
        int prod1Id = prod1.getIdProducto();
        int prod2Id = prod2.getIdProducto();
        int prod3Id = prod3.getIdProducto();
        
        Producto dbProd1 = em.find(Producto.class, prod1Id);
        System.out.println("Recuperando datos del Porducto 1 (UNO):");
        System.out.println("\t Id: " + dbProd1.getIdProducto());
        System.out.println("\t Descripción: " + dbProd1.getDescripcion());
        System.out.println("\t Precio: " + dbProd1.getPrecio());
        
        Producto dbProd2 = em.find(Producto.class, prod2Id);
        System.out.println("Recuperando datos del Porducto 2 (DOS):");
        System.out.println("\t Id: " + dbProd2.getIdProducto());
        System.out.println("\t Descripción: " + dbProd2.getDescripcion());
        System.out.println("\t Precio: " + dbProd2.getPrecio());
        
        Producto dbProd3 = em.find(Producto.class, prod3Id);
        System.out.println("Recuperando datos del Porducto 3 (TRES):");
        System.out.println("\t Id: " + dbProd3.getIdProducto());
        System.out.println("\t Descripción: " + dbProd3.getDescripcion());
        System.out.println("\t Precio: " + dbProd3.getPrecio());
        
        System.out.println("=========== IMPRIMIENDO CATALOGO DE ORDENES =========== ");
        
        int ordn1Id = orden1.getIdOrden();
        int ordn2Id = orden2.getIdOrden();

        Orden dborden1 = em.find(Orden.class, ordn1Id);
        System.out.println("Recuperando datos de la Orden 1(UNO)");
        System.out.println("\t Id: " + dborden1.getIdOrden());
        System.out.println("\t Fecha: " + dborden1.getFechaOrden());
        
        Orden dborden2 = em.find(Orden.class, ordn2Id);
        System.out.println("Recuperando datos de la Orden 2(DOS)");
        System.out.println("\t Id: " + dborden2.getIdOrden());
        System.out.println("\t Fecha: " + dborden2.getFechaOrden());
        
        System.out.println("=========== IMPRIMIENDO LINEAS DE ORDEN =========== ");
        LineaordenPK lord1= lineord1.getLineaordenPK();
        Lineaorden dblineaordn1=em.find(Lineaorden.class, lord1);
        System.out.println("Recuperando datos de la Linea De Orden 1(UNO)");
        System.out.println("\t" + dblineaordn1.getLineaordenPK());        
        System.out.println("\t" + dblineaordn1.getOrden());
        System.out.println("\t" + dblineaordn1.getProducto());
        System.out.println("\t Cantidad: " + dblineaordn1.getCantidad());
        
        LineaordenPK lord2= lineord2.getLineaordenPK();
        Lineaorden dblineaordn2=em.find(Lineaorden.class, lord2);
        System.out.println("Recuperando datos de la Linea De Orden 2(DOS)");
        System.out.println("\t" + dblineaordn2.getLineaordenPK());        
        System.out.println("\t" + dblineaordn2.getOrden());
        System.out.println("\t" + dblineaordn2.getProducto());
        System.out.println("\t Cantidad: " + dblineaordn2.getCantidad());
        
        LineaordenPK lord3= lineord3.getLineaordenPK();
        Lineaorden dblineaordn3=em.find(Lineaorden.class, lord3);
        System.out.println("Recuperando datos de la Linea De Orden 3(TRES)");
        System.out.println("\t" + dblineaordn3.getLineaordenPK());        
        System.out.println("\t" + dblineaordn3.getOrden());
        System.out.println("\t" + dblineaordn3.getProducto());
        System.out.println("\t Cantidad: " + dblineaordn3.getCantidad());
        
        LineaordenPK lord4= lineord4.getLineaordenPK();
        Lineaorden dblineaordn4=em.find(Lineaorden.class, lord4);
        System.out.println("Recuperando datos de la Linea De Orden 4(CUATRO)");
        System.out.println("\t" + dblineaordn4.getLineaordenPK());        
        System.out.println("\t" + dblineaordn4.getOrden());
        System.out.println("\t" + dblineaordn4.getProducto());
        System.out.println("\t Cantidad: " + dblineaordn4.getCantidad());
        
        try {
            em.getTransaction().commit();
        } catch (Exception e) {
            System.out.println("ERROR"+ e.getMessage());

        }

        em.close();
        emf.close();
        
       
        
    }
    
}
